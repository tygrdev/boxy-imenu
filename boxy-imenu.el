;;; boxy-imenu.el --- View imenu in a boxy diagram -*- lexical-binding: t -*-

;; Copyright (C) 2021-2023 Free Software Foundation, Inc.

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 0.0.7
;; File: boxy-imenu.el
;; Package-Requires: ((emacs "26.1") (boxy "1.1"))
;; Keywords: tools
;; URL: https://gitlab.com/grinn.amy/boxy-imenu

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; The command `boxy-imenu' will display `imenu' in a boxy diagram.
;;
;; Standard Emacs movement commands will navigate by boxes instead of
;; words or characters.  Clicking on a box or pressing RET will take
;; you to that item.  The 'o' command will open the item in the other
;; window.

;;; Code:

;;;; Requirements

(require 'boxy)
(require 'eieio)
(require 'imenu)

;;;; Options

(defgroup boxy-imenu nil
  "Customization options for boxy-imenu."
  :group 'applications)

(defcustom boxy-imenu-margin-x 0
  "Horizontal margin to be used when displaying boxes."
  :type 'number)

(defcustom boxy-imenu-margin-y 0
  "Vertical margin to be used when displaying boxes."
  :type 'number)

(defcustom boxy-imenu-padding-x 2
  "Horizontal padding to be used when displaying boxes."
  :type 'number)

(defcustom boxy-imenu-padding-y 0
  "Vertical padding to be used when displaying boxes."
  :type 'number)

(defcustom boxy-imenu-include-context t
  "Whether to show context when opening a real link."
  :type 'boolean)

(defcustom boxy-imenu-flex-width 80
  "When merging links, try to keep width below this."
  :type 'number)

(defcustom boxy-imenu-default-visibility 2
  "Default level to display boxes."
  :type 'number)

(defcustom boxy-imenu-tooltips t
  "Show tooltips in a boxy diagram."
  :type 'boolean)

(defcustom boxy-imenu-tooltip-timeout 0.5
  "Idle time before showing tooltip in a boxy diagram."
  :type 'number)

(defcustom boxy-imenu-tooltip-max-width 30
  "Maximum width of all tooltips."
  :type 'number)

;;;; Faces

(defface boxy-imenu-default nil
  "Default face used in boxy mode.")

(defface boxy-imenu-primary
  '((((background dark)) (:foreground "turquoise"))
    (t (:foreground "dark cyan")))
  "Face for highlighting the name of a box.")

(defface boxy-imenu-selected
  '((t :foreground "light slate blue"))
  "Face for the current box border under cursor.")

(defface boxy-imenu-rel
  '((t :foreground "hot pink"))
  "Face for the box which is related to the box under the cursor.")

(defface boxy-imenu-tooltip
  '((((background dark)) (:background "gray30" :foreground "gray"))
    (t (:background "gainsboro" :foreground "dim gray")))
  "Face for tooltips in a boxy diagram.")

;;;; Pretty printing

(cl-defun boxy-imenu-pp (box
                       &key
                       (display-buffer-fn 'display-buffer-same-window)
                       (visibility boxy-imenu-default-visibility)
                       (max-visibility 3)
                       select
                       header
                       (default-margin-x boxy-imenu-margin-x)
                       (default-margin-y boxy-imenu-margin-y)
                       (default-padding-x boxy-imenu-padding-x)
                       (default-padding-y boxy-imenu-padding-y)
                       (flex-width boxy-imenu-flex-width)
                       (tooltips boxy-imenu-tooltips)
                       (tooltip-timeout boxy-imenu-tooltip-timeout)
                       (tooltip-max-width boxy-imenu-tooltip-max-width)
                       (default-face 'boxy-imenu-default)
                       (primary-face 'boxy-imenu-primary)
                       (tooltip-face 'boxy-imenu-tooltip)
                       (rel-face 'boxy-imenu-rel)
                       (selected-face 'boxy-imenu-selected))
  "Pretty print BOX in a popup buffer.

If HEADER is passed in, it will be printed above the diagram.

DISPLAY-BUFFER-FN is used to display the diagram, by
default `display-buffer-pop-up-window'.

If SELECT is non-nil, select the boxy window after displaying
it.

VISIBILITY is the initial visibility of children and
MAX-VISIBILITY is the maximum depth to display when cycling
visibility.

DEFAULT-MARGIN-X, DEFAULT-MARGIN-Y, DEFAULT-PADDING-X and
DEFAULT-PADDING-Y will be the fallback values to use if a box's
margin and padding slots are not set.

When adding boxes, boxy will try to keep the width below
FLEX-WIDTH.

If TOOLTIPS is nil, don't show any tooltips.

TOOLTIP-TIMEOUT is the idle time to wait before showing a
tooltip.

TOOLTIP-MAX-WIDTH is the maximum width of a tooltip.  Lines
longer than this will be truncated.

DEFAULT-FACE, PRIMARY-FACE, TOOLTIP-FACE, REL-FACE, and
SELECTED-FACE can be set to change the appearance of the boxy
diagram."
  (boxy-pp box
           :display-buffer-fn display-buffer-fn
           :visibility visibility
           :max-visibility max-visibility
           :select select
           :header header
           :default-margin-x default-margin-x
           :default-margin-y default-margin-y
           :default-padding-x default-padding-x
           :default-padding-y default-padding-y
           :flex-width flex-width
           :tooltips tooltips
           :tooltip-timeout tooltip-timeout
           :tooltip-max-width tooltip-max-width
           :default-face default-face
           :primary-face primary-face
           :tooltip-face tooltip-face
           :rel-face rel-face
           :selected-face selected-face))

;;;; Commands

;;;###autoload
(defun boxy-imenu ()
  "View imenu as a boxy diagram."
  (interactive)
  (boxy-imenu-pp (boxy-imenu-make-world)
                 :select t))

;;;; Boxy implementation

(defun boxy-imenu-make-world (&optional buffer)
  "Create world from imenu index alist from BUFFER."
  (let* ((buff (or buffer (current-buffer)))
         (index-alist (with-current-buffer buff
                             (imenu--make-index-alist)))
         (world (boxy-box :level 1))
         (rescan (boxy-box :name "*Rescan*")))
    (oset rescan action
          `(lambda ()
             (interactive)
             (setq boxy--world (boxy-imenu-make-world ,buff))
             (boxy-mode-update-visibility)
             (boxy-mode-redraw)))
    (boxy-add-next rescan world)
    (mapc
     (lambda (item)
       (boxy-imenu--add-item world item buff))
     index-alist)
    world))

(defun boxy-imenu--add-item (prev item buffer)
  "Add ITEM to PREV boxy-box."
  (let ((box (boxy-box :name (car item) :rel "in")))
    (if (listp (cdr item))
        (progn
          (object-add-to-list box :expand-children
                              `(lambda (box)
                                 (mapc
                                  (lambda (item)
                                    (boxy-imenu--add-item box item ,buffer))
                                  ',(cdr item))))
          (let* ((markers (save-window-excursion
                            (switch-to-buffer buffer)
                            (boxy-imenu--get-markers item)))
                 (min-marker (seq-reduce
                              (lambda (min cur)
                                (if (< cur min) cur min))
                              markers
                              1.0e+INF))
                 (box-marker (save-window-excursion
                               (switch-to-buffer buffer)
                               (save-excursion
                                 (goto-char (marker-position min-marker))
                                 (let ((case-fold-search nil))
                                   (re-search-backward (regexp-quote (car item)) nil t))
                                 (point-marker)))))
            (push box-marker markers)
            (oset box markers markers)))
      (oset box primary t)
      (oset box markers (list (if (markerp (cdr item))
                                  (cdr item)
                                (save-window-excursion
                                  (switch-to-buffer buffer)
                                  (save-excursion
                                    (goto-char (cdr item))
                                    (point-marker)))))))
    (boxy-add-next box prev)))

;;;; Utility expressions

(defun boxy-imenu--get-markers (item)
  "Get all markers from imenu index-alist ITEM."
  (if (listp (cdr item))
      (apply 'append (mapcar #'boxy-imenu--get-markers (cdr item)))
    (list (if (markerp (cdr item))
              (cdr item)
            (save-excursion
              (goto-char (cdr item))
              (point-marker))))))

(provide 'boxy-imenu)
;;; boxy-imenu.el ends here
